/**
 * Created by Thomas on 5/3/15.
 */


// initialize states and cities info
var fs = require('fs');
var file = fs.readFileSync('./states_cities.json');
var states_cities = JSON.parse(file);


// logic to find relevant cities
var find = function(state, searchTerm) {

    var cities = states_cities[state];

    searchTerm = searchTerm.toLowerCase();
    var matchFilter = function(city) {

        return city.substr(0, searchTerm.length).toLowerCase() == searchTerm;
    };

    return cities.filter(matchFilter);
};


// initialize web server
var express = require('express');
var app = express();

/**
 * @apiVersion 0.1.0
 *
 * @apiGroup State
 * @apiName State - Get cities for state.
 *
 * @api {get} /state/:state
 * @apiExample {curl} Example usage:
 *     curl -i http://cityinfo.us/state/ca?query=san
 *
 * @apiParam {String} state Mandatory
 * @apiParam {String} query Optional
 *
 * @apiSuccess {Array} cities List of matching cities.
 *
 * @apiError InvalidState Is not a valid state.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.0 200 OK
 * [
 *      "Apple Valley"
 * ]
 *
 * @apiSampleRequest http://cityinfo.us/state/ca?query=san
 */
app.get('/state/:state', function(req, resp) {

    var state = req.params.state == null ? null : req.params.state.toUpperCase();

    var searchTerm = req.query.query;
    var callback = req.query.callback;

    if (state in states_cities) {

        var results = searchTerm == null ? states_cities[state] : find(state, searchTerm);
        if (callback == null) {
            resp.type('json');
            resp.send(results);
        } else {
            resp.type('application/javascript');
            resp.send(callback + '(' + JSON.stringify(results) + ');')
        }
    } else {

        resp.status(400).send('Bad Request, "' + state + '" Is Not A Valid State');
    }
});

app.use(express.static('apidoc'));

var port = process.env.LISTEN === undefined ? 4444 : process.env.LISTEN;
app.listen(port);
