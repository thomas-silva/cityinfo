define({
  "name": "cityinfo",
  "version": "0.1.0",
  "description": "CityInfo REST API",
  "title": "CityInfo REST API",
  "url": "http://cityinfo.us",
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2015-05-05T21:51:24.352Z",
    "url": "http://apidocjs.com",
    "version": "0.12.3"
  }
});