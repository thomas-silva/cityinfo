define({ "api": [
  {
    "version": "0.1.0",
    "group": "State",
    "name": "State___Get_cities_for_state_",
    "type": "get",
    "url": "/state/:state",
    "title": "",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://cityinfo.us/state/ca?query=san",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "state",
            "description": "<p>Mandatory</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "query",
            "description": "<p>Optional</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "cities",
            "description": "<p>List of matching cities.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.0 200 OK\n[\n     \"Apple Valley\"\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidState",
            "description": "<p>Is not a valid state.</p> "
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://cityinfo.us/state/ca?query=san"
      }
    ],
    "filename": "cityinfo/app.js",
    "groupTitle": "State"
  }
] });